% 2D thermo-mechanical viscous code;
% solution of 2D Stokes, continuity, temperature and advection equations 
% with finite differences and marker-in-cell technique 
% on a regular grid using pressure-velocity formulation
% for a deforming incompressible medium 
% with variable viscosity and thermal conductivity
% taking into account radiogenic, shear and adiabatic heating

% Clear all variables, close all figure and clear the screen
clear all; close all; clc;

display('*** Start 2D thermo-mechanical code ***')
display(' ')


% Numerical model parameters
% Model size [m]
xsize=???;         % Horizontal
ysize=???;         % Vertical

% Numbers of nodes
xnum=???; % Horizontal
ynum=???;  % Vertical
% Grid step
xstp=xsize/(xnum-1); % Horizontal
ystp=ysize/(ynum-1); % Vertical

% Maximal length of timestep [s]
timemax=1.000e+6*365.25*24.000*3600.000; % 1 Ma
% Maximal marker displacement step, number of gridsteps
markmax=0.5;
% Moving Markers: 
% 0 = not moving at all
% 1 = simple 1-st order advection
% 4 = 4-th order in space Runge-Kutta
markmove=4;
% Amount of timesteps
stepmax=1000;
% Subgrid diffusion coefficient
dsubgridt=1;
% Maximal temperature change, allowed for one timestep, K
tempmax=20;

% Material properties
% Viscosity [Pa s]
MFLOW(1)=???;  % 1 = overburden
MFLOW(2)=???;  % 2 = bottom layer
% Density [kg/m^3]
MRHO(1)=???;    % 1 = overburden
MRHO(2)=???;    % 2 = bottom layer
% Thermal conductivity [W/(m K)]
MKT(1)=???;        % 1 = overburden
MKT(2)=???;        % 2 = bottom layer
% Thermal expansion [1/K]
MALPHA(1)=???;  % 1 = overburden
MALPHA(2)=???;  % 2 = bottom layer
% Radioactive heating [W/m^3]
MHR(1)=???;     % 1 = overburden
MHR(2)=???;     % 2 = bottom layer
% Heat capacity [J/kg]
MCP(1)=???;     % 1 = overburden
MCP(2)=???;     % 2 = bottom layer


% Model temperature [K]
tktop=???;      % overburden
tkbottom=???;   % bottom layer


% Pressure condition in one cell (i==2 && j==3)
p0cell=0;

% Gravity acceleration directed downward [m/s^2]
gy=???;

% Making vectors for nodal points positions (basic nodes)
gridx=0:xstp:xsize; % Horizontal
gridy=0:ystp:ysize; % Vertical
% Making vectors for cell centers positions (staggered nodes)
gridcx=xstp/2:xstp:xsize-xstp/2; % Horizontal
gridcy=ystp/2:ystp:ysize-ystp/2; % Vertical

% Defining number of markers and steps between them in the horizontal and vertical direction
mxnum=???;  %total number of markers in horizontal direction
mynum=???;  %total number of markers in vertical direction
mxstep=xsize/mxnum; %step between markers in horizontal direction   
mystep=ysize/mynum; %step between markers in vertical direction

% Creating markers arrays
MX=zeros(mynum*mxnum,1);    % X coordinate [m]
MY=zeros(mynum*mxnum,1);    % Y coordinate [m]
MI=zeros(mynum*mxnum,1);    % Type
MTK=zeros(mynum*mxnum,1);   % Temperature [K]

% Defining initial position of markers
% Defining lithological structure of the model
% Marker counter
mm1=0;
for xm = 1:1:mxnum
    for ym = 1:1:mynum
        
        % Update marker counter:
        mm1=mm1+1;
        
        % Coordinates with small random displacement
        MX(mm1)=xm*mxstep-mxstep/2+(rand-0.5)*mxstep;
        MY(mm1)=ym*mystep-mystep/2+(rand-0.5)*mystep;
        
        % Define overburden
        MI(mm1)=1;
        MTK(mm1)=tktop;
        
    end
end

% Save Number of markers
marknum=mm1;

% Density, viscosity arrays
etas = zeros(ynum,xnum);    % Viscosity for shear stress
etan = zeros(ynum,xnum);    % Viscosity for normal stress
rho = zeros(ynum,xnum);     % Density
tk1 = zeros(ynum,xnum);     % Temperature
hr = zeros(ynum,xnum);      % radiogenic heating
ha = zeros(ynum,xnum);      % adiabatic heating
kt = zeros(ynum,xnum);      % thermal conductivity
rhocp = zeros(ynum,xnum);   % RHO*CP

% Initial time, s
timesum=0;

% Main Time cycle
for ntimestep=1:1:stepmax
    
    display(['Step no.: ',num2str(ntimestep)])
    
    % Backup transport properties arrays
    etas0 = etas;
    etan0 = etan;
    rho0 = rho;
    tk0 = tk1;
    hr0 = hr;
    ha0 = ha;
    kt0 = kt;
    rhocp0 = rhocp;
    % Clear transport properties arrays
    etas = zeros(ynum,xnum);   % Viscosity for shear stress
    etan = zeros(ynum,xnum);   % Viscosity for normal stress
    rho = zeros(ynum,xnum);    % Density
    tk1 = zeros(ynum,xnum);    % Temperature
    hr = zeros(ynum,xnum);     % radiogenic heating
    ha = zeros(ynum,xnum);     % adiabatic heating
    kt = zeros(ynum,xnum);     % thermal conductivity
    rhocp = zeros(ynum,xnum);  % RHO*CP
    % Clear weights for basic nodes
    wtnodes=zeros(ynum,xnum);
    % Clear weights for etas
    wtetas=zeros(ynum,xnum);
    % Clear weights for etan
    wtetan=zeros(ynum,xnum);

    % Interpolating parameters from markers to nodes
    for mm1 = 1:1:marknum

        % Check markers inside the grid
        if (MX(mm1)>=gridx(1) && MX(mm1)<=gridx(xnum) && MY(mm1)>=gridy(1) && MY(mm1)<=gridy(ynum)) 

            %  xn    rho(yn,xn)--------------------rho(yn,xn+1)
            %           ?           ^                  ?
            %           ?           ?                  ?
            %           ?          dy                  ?
            %           ?           ?                  ?
            %           ?           v                  ?
            %           ?<----dx--->o MRHO(mm1)        ?
            %           ?                              ?
            %           ?                              ?
            %  xn+1  rho(yn+1,xn)-------------------rho(yn+1,xn+1)
            %
            % Define indexes for upper left node in the cell where the marker is
            % !!! SUBTRACT 0.5 since int16(0.5)=1
            xn=double(int16(MX(mm1)./xstp-0.5))+1;
            yn=double(int16(MY(mm1)./ystp-0.5))+1;
            if (xn<1)
                xn=1;
            end
            if (xn>xnum-1)
                xn=xnum-1;
            end
            if (yn<1)
                yn=1;
            end
            if (yn>ynum-1)
                yn=ynum-1;
            end

            % Define normalized distances from marker to the upper left node;
            dx=(MX(mm1)-gridx(xn))./xstp;
            dy=(MY(mm1)-gridy(yn))./ystp;

            % Define material density and viscosity from marker type
            if(MI(mm1)==1)
                MRHOCUR=MRHO(MI(mm1)); % Density of overburden
            elseif(MI(mm1)>1)    
                MRHOCUR=MRHO(MI(mm1))*(1.000-MALPHA(2)*(MTK(mm1)-298.15)); % Density of bottom layer
            end 
            METACUR=MFLOW(MI(mm1)); % Viscosity

            % Add RHO, T, ALPHA*T, Hr, RHO*CP and K to 4 surrounding basic nodes
            % Upper-Left node
            rho(yn,xn)=rho(yn,xn)+(1.0-dx).*(1.0-dy).*MRHOCUR;
            tk1(yn,xn)=tk1(yn,xn)+(1.0-dx).*(1.0-dy).*MTK(mm1);
            hr(yn,xn)=hr(yn,xn)+(1.0-dx).*(1.0-dy).*MHR(MI(mm1));
            ha(yn,xn)=ha(yn,xn)+(1.0-dx).*(1.0-dy).*MTK(mm1)*MALPHA(MI(mm1));
            rhocp(yn,xn)=rhocp(yn,xn)+(1.0-dx).*(1.0-dy).*MRHOCUR*MCP(MI(mm1));
            kt(yn,xn)=kt(yn,xn)+(1.0-dx).*(1.0-dy).*MKT(MI(mm1));
            wtnodes(yn,xn)=wtnodes(yn,xn)+(1.0-dx).*(1.0-dy);
            % Lower-Left node
            rho(yn+1,xn)=rho(yn+1,xn)+(1.0-dx).*dy.*MRHOCUR;
            tk1(yn+1,xn)=tk1(yn+1,xn)+(1.0-dx).*dy.*MTK(mm1);
            hr(yn+1,xn)=hr(yn+1,xn)+(1.0-dx).*dy.*MHR(MI(mm1));
            ha(yn+1,xn)=ha(yn+1,xn)+(1.0-dx).*dy.*MTK(mm1)*MALPHA(MI(mm1));
            rhocp(yn+1,xn)=rhocp(yn+1,xn)+(1.0-dx).*dy.*MRHOCUR*MCP(MI(mm1));
            kt(yn+1,xn)=kt(yn+1,xn)+(1.0-dx).*dy.*MKT(MI(mm1));
            wtnodes(yn+1,xn)=wtnodes(yn+1,xn)+(1.0-dx).*dy;
            % Upper-Right node
            rho(yn,xn+1)=rho(yn,xn+1)+dx.*(1.0-dy).*MRHOCUR;
            tk1(yn,xn+1)=tk1(yn,xn+1)+dx.*(1.0-dy).*MTK(mm1);
            hr(yn,xn+1)=hr(yn,xn+1)+dx.*(1.0-dy).*MHR(MI(mm1));
            ha(yn,xn+1)=ha(yn,xn+1)+dx.*(1.0-dy).*MTK(mm1)*MALPHA(MI(mm1));
            rhocp(yn,xn+1)=rhocp(yn,xn+1)+dx.*(1.0-dy).*MRHOCUR*MCP(MI(mm1));
            kt(yn,xn+1)=kt(yn,xn+1)+dx.*(1.0-dy).*MKT(MI(mm1));
            wtnodes(yn,xn+1)=wtnodes(yn,xn+1)+dx.*(1.0-dy);
            % Lower-Right node
            rho(yn+1,xn+1)=rho(yn+1,xn+1)+dx.*dy.*MRHOCUR;
            tk1(yn+1,xn+1)=tk1(yn+1,xn+1)+dx.*dy.*MTK(mm1);
            hr(yn+1,xn+1)=hr(yn+1,xn+1)+dx.*dy.*MHR(MI(mm1));
            ha(yn+1,xn+1)=ha(yn+1,xn+1)+dx.*dy.*MTK(mm1)*MALPHA(MI(mm1));
            rhocp(yn+1,xn+1)=rhocp(yn+1,xn+1)+dx.*dy.*MRHOCUR*MCP(MI(mm1));
            kt(yn+1,xn+1)=kt(yn+1,xn+1)+dx.*dy.*MKT(MI(mm1));
            wtnodes(yn+1,xn+1)=wtnodes(yn+1,xn+1)+dx.*dy;

            % Add viscosity etas() to 4 surrounding basic nodes
            % only using markers located at <=0.5 gridstep distances from nodes
            % Upper-Left node
            if(dx<=0.5 && dy<=0.5)
                etas(yn,xn)=etas(yn,xn)+(1.0-dx).*(1.0-dy).*METACUR;
                wtetas(yn,xn)=wtetas(yn,xn)+(1.0-dx).*(1.0-dy);
            end
            % Lower-Left node
            if(dx<=0.5 && dy>=0.5)
                etas(yn+1,xn)=etas(yn+1,xn)+(1.0-dx).*dy.*METACUR;
                wtetas(yn+1,xn)=wtetas(yn+1,xn)+(1.0-dx).*dy;
            end
            % Upper-Right node
            if(dx>=0.5 && dy<=0.5)
                etas(yn,xn+1)=etas(yn,xn+1)+dx.*(1.0-dy).*METACUR;
                wtetas(yn,xn+1)=wtetas(yn,xn+1)+dx.*(1.0-dy);
            end
            % Lower-Right node
            if(dx>=0.5 && dy>=0.5)
                etas(yn+1,xn+1)=etas(yn+1,xn+1)+dx.*dy.*METACUR;
                wtetas(yn+1,xn+1)=wtetas(yn+1,xn+1)+dx.*dy;
            end

            % Add viscosity etan() to the center of current cell (pressure node)
            etan(yn+1,xn+1)=etan(yn+1,xn+1)+(1.0-abs(0.5-dx)).*(1.0-abs(0.5-dy)).*METACUR;
            wtetan(yn+1,xn+1)=wtetan(yn+1,xn+1)+(1.0-abs(0.5-dx)).*(1.0-abs(0.5-dy));
        end

    end

    % Computing  Viscosity, density, rock type for nodal points
    for i=1:1:ynum;
        for j=1:1:xnum;
            % Density
            if (wtnodes(i,j)~=0)
                % Compute new value interpolated from markers
                rho(i,j)=rho(i,j)./wtnodes(i,j);
                tk1(i,j)=tk1(i,j)./wtnodes(i,j);
                hr(i,j)=hr(i,j)./wtnodes(i,j);
                ha(i,j)=ha(i,j)./wtnodes(i,j);
                rhocp(i,j)=rhocp(i,j)./wtnodes(i,j);
                kt(i,j)=kt(i,j)./wtnodes(i,j);
            else
                % If no new value is interpolated from markers old value is used
                rho(i,j)=rho0(i,j);
                tk1(i,j)=tk0(i,j);
                hr(i,j)=hr0(i,j);
                ha(i,j)=ha0(i,j);
                rhocp(i,j)=rhocp0(i,j);
                kt(i,j)=kt0(i,j);
            end
            % Viscosity etas() (basic nodes)
            if (wtetas(i,j)~=0)
                % Compute new value interpolated from markers
                etas(i,j)=etas(i,j)./wtetas(i,j);
            else
                % If no new value is interpolated from markers old value is used
                etas(i,j)=etas0(i,j);
            end
            % Viscosity etan() (pressure cells)
             if (wtetan(i,j)~=0)
                % Compute new value interpolated from markers
                etan(i,j)=etan(i,j)./wtetan(i,j);
            else
                % If no new value is interpolated from markers old value is used
                etan(i,j)=etan0(i,j);
            end
        end
    end

    % Applying thermal boundary conditions for interpolated temperature
    % Upper, Lower boundaries
    for j=2:1:xnum-1
        % Upper boundary: 
         tk1(1,j)            =   tktop; % Constant temperature
        %tk1(1,j)            =   tk1(2,j); % Insulating boundary
        % Lower boundary: 
         tk1(ynum,j)         =   tktop; % Constant temperature
        %tk1(ynum,j)            =   tk1(ynum-1,j); % Insulating boundary
    end
    % Left, Right boundaries: constant temperature
    for i=1:1:ynum
        % Left boundary: 
         tk1(i,1)            =   tktop; % Constant temperature
        %tk1(i,1)            =   tk1(i,2); % Insulating boundary
        % Right boundary: 
         tk1(i,xnum)         =   tktop; % Constant temperature
        %tk1(i,xnum)            =   tk1(i,xnum-1); % Insulating boundary
    end
   
    % Re-interpolating initial nodal temperatures back to markers
    % to avoid initial discrepancies between markers and nodes 
    if (ntimestep==1)
        % Marker cycle
        for mm1 = 1:1:marknum
            % Check markers inside the grid
            if (MX(mm1)>=gridx(1) && MX(mm1)<=gridx(xnum) && MY(mm1)>=gridy(1) && MY(mm1)<=gridy(ynum)) 
                %  xn    tk1(yn,xn)--------------------tk1(yn,xn+1)
                %           ?           ^                  ?
                %           ?           ?                  ?
                %           ?          dy                  ?
                %           ?           ?                  ?
                %           ?           v                  ?
                %           ?<----dx--->o MTK(mm1)         ?
                %           ?                              ?
                %           ?                              ?
                %  xn+1  tk1(yn+1,xn)-------------------tk1(yn+1,xn+1)
                %
                %
                % Define indexes for upper left node in the cell where the marker is
                % !!! SUBTRACT 0.5 since int16(0.5)=1
                xn=double(int16(MX(mm1)./xstp-0.5))+1;
                yn=double(int16(MY(mm1)./ystp-0.5))+1;
                if (xn<1)
                    xn=1;
                end
                if (xn>xnum-1)
                    xn=xnum-1;
                end
                if (yn<1)
                    yn=1;
                end
                if (yn>ynum-1)
                    yn=ynum-1;
                end
                % Define normalized distances from marker to the upper left node;
                dx=(MX(mm1)-gridx(xn))./xstp;
                dy=(MY(mm1)-gridy(yn))./ystp;
                % Interpolate nodal temperature for the marker
                tkm=0;
                tkm=tkm+(1.0-dx).*(1.0-dy).*tk1(yn,xn);
                tkm=tkm+(1.0-dx).*dy.*tk1(yn+1,xn);
                tkm=tkm+dx.*(1.0-dy).*tk1(yn,xn+1);
                tkm=tkm+dx.*dy.*tk1(yn+1,xn+1);
                % Reset marker temperature
                MTK(mm1)=tkm;
            end
        end
    end    
    
    
    if(ntimestep==1)
        display('Visualize initial condition')
    else
        display('Visualize data from previous timestep')
    end
    
    figure(1), clf;
    
    % Plotting temperature
    subplot(2,2,1)
    pcolor(gridx/1000,gridy/1000,tk1);
    shading interp;
    axis tight;
    title(['{\itT } [K]: Step=',num2str(ntimestep-1),' Time = ',num2str(timesum*1e-6/(365.25*24*3600)),' Ma']);
    colorbar;
    caxis([tktop tkbottom]);
    xlabel('x [km]');
    ylabel('y [km]');
    axis ij image ;
    axis([0 xsize/1000 0 ysize/1000]);
    
    % Plotting thermal conductivity
    subplot(2,2,2)
    pcolor(gridx/1000,gridy/1000,kt);
    shading interp;
    axis tight;
    title('{\itk } [W/(m K)]');
    colorbar; 
    xlabel('x [km]');
    ylabel('y [km]');
    axis ij image ;
    axis([0 xsize/1000 0 ysize/1000]);
    
    % Plotting viscosity
    subplot(2,2,3)
    pcolor(gridx/1000,gridy/1000,log10(etas));
    shading interp;
    axis tight;
    title('log_{10}(viscosity/[Pa s])');
    colorbar;
    xlabel('x [km]');
    ylabel('y [km]');
    axis ij image ;
    axis([0 xsize/1000 0 ysize/1000]);
    
    % Plotting density
    subplot(2,2,4)
    pcolor(gridx/1000,gridy/1000,rho);
    shading interp;
    axis tight;
    title('Density {\it\rho } [kg/m^3]');
    colorbar;
    caxis([2900 3500]);
    xlabel('x [km]');
    ylabel('y [km]');
    axis ij image ;
    axis([0 xsize/1000 0 ysize/1000]);
    
    % Set background color to white
    set(gcf, 'color', 'white');

    % Save results in figure
    print ('-dpng', '-r500',['rt_step_',num2str(ntimestep-1),'.png']);   

    
    % Matrix of coefficients initialization
    L=sparse(xnum*ynum*3,xnum*ynum*3);
    % Vector of right part initialization
    R=zeros(xnum*ynum*3,1);

    % Computing Kcont and Kbond coefficients 
    etamin=min(min(etas)); % min viscosity in the model
    kcont=2*etamin/(xstp+ystp);
    kbond=4*etamin/(xstp+ystp)^2;

    % Solving x-Stokes, y-Stokes and continuity equations
    % x-Stokes: ETA(d2vx/dx2+d2vx/dy2)-dP/dx=0
    % y-Stokes: ETA(d2vy/dx2+d2vy/dy2)-dP/dy=gy*RHO
    % continuity: dvx/dx+dvy/dy=0
    % Composing matrix of coefficients L()
    % and vector (column) of right parts R()
    % Boundary conditions: free slip
    % Process all Grid points
    for i=1:1:ynum
      for j=1:1:xnum
        % Global index for P, vx, vy in the current node
        inp=((j-1)*ynum+i)*3-2; % P
        invx=inp+1;
        invy=inp+2;


        % Continuity equation
        % Ghost pressure unknowns (i=1, j=1) and boundary nodes (4 corners + one cell)
        if(i==1 || j==1 || (i==2 && j==2) || (i==2 && j==xnum) || (i==ynum && j==2) || (i==ynum && j==xnum) || (i==2 && j==3))
            % Ghost pressure unknowns (i=1, j=1): P(i,j)=0
            if(i==1 || j==1)
                L(inp,inp)          =   1*kbond;            % Coefficient for P(i,j)
                R(inp,1)            =   0;                  % Right part
            end
            % Upper and lower left corners dP/dx=0 => P(i,j)-P(i,j+1)=0
            if((i==2 && j==2) || (i==ynum && j==2))
                L(inp,inp)          =   1*kbond;            % Coefficient for P(i,j) 
                L(inp,inp+ynum*3)   =   -1*kbond;           % Coefficient for P(i,j+1)
                R(inp,1)            =   0;                  % Right part
            end
            % Upper and lower right corners dP/dx=0 => P(i,j)-P(i,j-1)=0
            if((i==2 && j==xnum) || (i==ynum && j==xnum))
                L(inp,inp)          =   1*kbond;            % Coefficient for P(i,j) 
                L(inp,inp-ynum*3)   =   -1*kbond;           % Coefficient for P(i,j-1)
                R(inp,1)            =   0;                  % Right part
            end
            % One cell 
            if (i==2 && j==3)
                L(inp,inp)          =   1*kbond;            % Coefficient for P(i,j)
                R(inp,1)            =   p0cell;             % Right part
            end
        %Internal nodes: dvx/dx+dvy/dy=0
        else
            %dvx/dx=(vx(i-1,j)-vx(i-1,j-1))/dx
            L(inp,invx-3)           =   kcont/xstp;         % Coefficient for vx(i-1,j) 
            L(inp,invx-3-ynum*3)    =   -kcont/xstp;        % Coefficient for vx(i-1,j-1) 
            %dvy/dy=(vy(i,j-1)-vy(i-1,j-1))/dy
            L(inp,invy-ynum*3)      =   kcont/ystp;         % Coefficient for vy(i,j-1) 
            L(inp,invy-3-ynum*3)    =   -kcont/ystp;        % Coefficient for vy(i-1,j-1) 
            % Right part:0
            R(inp,1)=0;
        end

        % x-Stokes equation
        % Ghost vx unknowns (i=ynum) and boundary nodes (i=1, i=ynum-1, j=1, j=xnum)
        if(i==1 || i==ynum-1 || i==ynum || j==1 || j==xnum)
            % Ghost vx unknowns (i=ynum: vx(i,j)=0
            if(i==ynum)
                L(invx,invx)        =   1*kbond; % Coefficient for vx(i,j)
                R(invx,1)           =   0; % Right part
            end
            % Left and Right boundaries (j=1, j=xnum) 
            if((j==1 || j==xnum) && i<ynum)
                % Free slip, No slip: vx(i,j)=0
                L(invx,invx)        =   1*kbond; % Coefficient for vx(i,j)
                R(invx,1)           =   0; % Right part
            end
            % Upper boundary, iner points (i=1, 1<j<xnum)
            if(i==1 && j>1 && j<xnum)
                % Free slip dvx/dy=0: vx(i,j)-vx(i+1,j)=0
                L(invx,invx)        =   1*kbond; % Coefficient for vx(i,j)
                L(invx,invx+3)      =   -1*kbond; % Coefficient for vx(i+1,j)
                R(invx,1)=0; % Right part
    %             % No slip vx=0: vx(i,j)-1/3*vx(i+1,j)=0
    %             L(invx,invx)=1*kbond; % Coefficient for vx(i,j)
    %             L(invx,invx+3)=-1/3*kbond; % Coefficient for vx(i+1,j)
    %             R(invx,1)=0; % Right part
            end
            % Lower boundary, iner points (i=ynum-1, 1<j<xnum)
            if(i==ynum-1 && j>1 && j<xnum)
                % Free slip dvx/dy=0: vx(i,j)-vx(i-1,j)=0
                L(invx,invx)        =   1*kbond; % Coefficient for vx(i,j)
                L(invx,invx-3)      =   -1*kbond; % Coefficient for vx(i-1,j)
                R(invx,1)=0; % Right part
    %             % No slip vx=0: vx(i,j)-1/3*vx(i+1,j)=0
    %             L(invx,invx)=1*kbond; % Coefficient for vx(i,j)
    %             L(invx,invx-3)=-1/3*kbond; % Coefficient for vx(i-1,j)
    %             R(invx,1)=0; % Right part
            end
        %Internal nodes: dSxx/dx+dSxy/dy-dP/dx=0
        else
            %dSxx/dx=2*etan(i+1,j+1)*(vx(i,j+1)-vx(i,j))/dx^2-2*etan(i+1,j)*(vx(i,j)-vx(i,j-1))/dx^2
            L(invx,invx+ynum*3)     =   2*etan(i+1,j+1)/xstp^2;                         % Coefficient for vx(i,j+1)
            L(invx,invx-ynum*3)     =   2*etan(i+1,j)/xstp^2;                           % Coefficient for vx(i,j-1)
            L(invx,invx)            =   -2*etan(i+1,j+1)/xstp^2-2*etan(i+1,j)/xstp^2;   % Coefficient for vx(i,j)

            %dSxy/dy=etas(i+1,j)*((vx(i+1,j)-vx(i,j))/dy^2+(vy(i+1,j)-vy(i+1,j-1))/dx/dy)-
            %         -etas(i,j)*((vx(i,j)-vx(i-1,j))/dy^2+(vy(i,j)-vy(i,j-1))/dx/dy)-
            L(invx,invx+3)          =   etas(i+1,j)/ystp^2;                             % Coefficient for vx(i+1,j)
            L(invx,invx-3)          =   etas(i,j)/ystp^2;                               % Coefficient for vx(i-1,j)
            L(invx,invx)            =   L(invx,invx)-etas(i+1,j)/ystp^2-etas(i,j)/ystp^2; % ADD coefficient for vx(i,j)
            L(invx,invy+3)          =   etas(i+1,j)/xstp/ystp;                          % Coefficient for vy(i+1,j)
            L(invx,invy+3-ynum*3)   =   -etas(i+1,j)/xstp/ystp;                         % Coefficient for vy(i+1,j-1)
            L(invx,invy)            =   -etas(i,j)/xstp/ystp;                           % Coefficient for vy(i,j)
            L(invx,invy-ynum*3)     =   etas(i,j)/xstp/ystp;                            % Coefficient for vy(i,j-1)
            % -dP/dx=(P(i+1,j)-P(i+1,j+1))/dx
            L(invx,inp+3)           =   kcont/xstp;                                     % Coefficient for P(i+1,j)
            L(invx,inp+3+ynum*3)    =   -kcont/xstp;                                    % Coefficient for P(i+1,j+1)
            % Right part:0
            R(invx,1)               =   0;
        end

        % y-Stokes equation
        % Ghost vy unknowns (j=xnum) and boundary nodes (i=1, i=ynum, j=1, j=xnum-1)
        if(i==1 || i==ynum || j==1 || j==xnum-1 || j==xnum)
            % Ghost vy unknowns (j=xnum: vy(i,j)=0
            if(j==xnum)
                L(invy,invy)        =   1*kbond;                                % Coefficient for vy(i,j)
                R(invy,1)           =   0;
            end
            % Upper and lower boundaries (i=1, i=ynum) 
            if((i==1 || i==ynum) && j<xnum)
                % Free slip, No slip: vy(i,j)=0
                L(invy,invy)        =   1*kbond;                                % Coefficient for vy(i,j)
                R(invy,1)           =   0;
            end
            % Left boundary, iner points (j=1, 1<i<ynum)
            if(j==1 && i>1 && i<ynum)
                % Free slip dvy/dx=0: vy(i,j)-vy(i,j+1)=0
                L(invy,invy)        =   1*kbond;                                % Coefficient for vy(i,j)
                L(invy,invy+ynum*3) =   -1*kbond;                               % Coefficient for vy(i,j+1)
                R(invy,1)           =   0;
    %             % No slip vy=0: vy(i,j)-1/3*vy(i,j+1)=0
    %             L(invy,invy)=1*kbond; % Coefficient for vy(i,j)
    %             L(invy,invy+ynum*3)=-1/3*kbond; % Coefficient for vy(i,j+1)
    %             R(invy,1)=0;
            end
            % Right boundary, iner points (j=xnum-1, 1<i<ynum)
            if(j==xnum-1 && i>1 && i<ynum)
                % Free slip dvy/dx=0: vy(i,j)-vy(i,j-1)=0
                L(invy,invy)        =   1*kbond;                                % Coefficient for vy(i,j)
                L(invy,invy-ynum*3) =  -1*kbond;                                % Coefficient for vy(i,j-1)
                R(invy,1)           =   0;
    %             % No slip vy=0: vy(i,j)-1/3*vy(i,j-1)=0
    %             L(invy,invy)=1*kbond; % Coefficient for vy(i,j)
    %             L(invy,invy-ynum*3)=-1/3*kbond; % Coefficient for vy(i,j-1)
    %             R(invy,1)=0;
            end
        %Internal nodes: dSyy/dy+dSxy/dx-dP/dy=-gy*RHO
        else
            %dSyy/dy=2*etan(i+1,j+1)*(vy(i+1,j)-vy(i,j))/dy^2-2*etan(i,j+1)*(vy(i,j)-vy(i-1,j))/dy^2
            L(invy,invy+3)          =   2*etan(i+1,j+1)/ystp^2;                 % Coefficient for vy(i+1,j)
            L(invy,invy-3)          =   2*etan(i,j+1)/ystp^2;                   % Coefficient for vy(i-1,j)
            L(invy,invy)            =   -2*etan(i+1,j+1)/ystp^2-2*etan(i,j+1)/ystp^2; % Coefficient for vy(i,j)

            %dSxy/dx=etas(i,j+1)*((vy(i,j+1)-vy(i,j))/dx^2+(vx(i,j+1)-vx(i-1,j+1))/dx/dy)-
            %         -etas(i,j)*((vy(i,j)-vy(i,j-1))/dx^2+(vx(i,j)-vx(i-1,j))/dx/dy)-
            L(invy,invy+ynum*3)     =   etas(i,j+1)/xstp^2;                     % Coefficient for vy(i,j+1)
            L(invy,invy-ynum*3)     =   etas(i,j)/xstp^2;                       % Coefficient for vy(i,j-1)
            L(invy,invy)            =   L(invy,invy)-etas(i,j+1)/xstp^2-etas(i,j)/xstp^2; % ADD coefficient for vy(i,j)
            L(invy,invx+ynum*3)     =   etas(i,j+1)/xstp/ystp;                  % Coefficient for vx(i,j+1)
            L(invy,invx+ynum*3-3)   =   -etas(i,j+1)/xstp/ystp;                 % Coefficient for vx(i-1,j+1)
            L(invy,invx)            =   -etas(i,j)/xstp/ystp;                   % Coefficient for vx(i,j)
            L(invy,invx-3)          =   etas(i,j)/xstp/ystp;                    % Coefficient for vx(i-1,j)

            % -dP/dy=(P(i,j+1)-P(i+1,j+1))/dx
            L(invy,inp+ynum*3)      =   kcont/ystp;                             % Coefficient for P(i,j+1)
            L(invy,inp+3+ynum*3)    =   -kcont/ystp;                            % Coefficient for P(i+1,j+1)
            % Right part: -RHO*gy
            R(invy,1)               =   -gy*(rho(i,j)+rho(i,j+1))/2;
        end

      end
    end

    %Obtaining vector of solutions S()
    S=L\R;

    % Reload solutions to 2D p(), vx(), vy() arrays
    % Dimensions of arrays are reduced compared to the basic grid
    p=zeros(ynum,xnum);
    vy=zeros(ynum,xnum);
    vx=zeros(ynum,xnum);
    % Process all Grid points
    for i=1:1:ynum
      for j=1:1:xnum
        % Global index for P, vx, vy in S()
        inp=((j-1)*ynum+i)*3-2; % P
        invx=inp+1;
        invy=inp+2;
        % P
        p(i,j)=S(inp)*kcont;
        % vx
        vx(i,j)=S(invx);
        % vy
        vy(i,j)=S(invy);
      end
    end


    % Compute vx,vy for internal nodes
    vx1=zeros(ynum,xnum);
    vy1=zeros(ynum,xnum);
    % Process internal Grid points
    for i=2:1:ynum-1
      for j=2:1:xnum-1
        % vx
        vx1(i,j)=(vx(i-1,j)+vx(i,j))/2;
         % vy
        vy1(i,j)=(vy(i,j-1)+vy(i,j))/2;
      end
    end


    % Compute strain.rate Exx and stress S'xx
    exx=zeros(ynum,xnum);
    sxx=zeros(ynum,xnum);
    % Process pressure cells
    for i=2:1:ynum
      for j=2:1:xnum
        % Exx, Sxx
         exx(i,j)=(vx(i-1,j)-vx(i-1,j-1))/xstp;
         sxx(i,j)=2*etan(i,j)*exx(i,j);
      end
    end

    % Compute strain-rates Exy, stress Sxy, shear heating and adiabatic heating
    exy=zeros(ynum,xnum);
    sxy=zeros(ynum,xnum);
    hs=zeros(ynum,xnum);
    % Process internal basic nodes
    for i=2:1:ynum-1
      for j=2:1:xnum-1
        % Exy, Sxy
         exy(i,j)=1/2*((vx(i,j)-vx(i-1,j))/ystp+(vy(i,j)-vy(i,j-1))/xstp);
         sxy(i,j)=2*etas(i,j)*exy(i,j);
         % Shear heating Hs=2S'xx*Exx+2Sxy*xy
         % S'xx*Exx term is averaged from four surrounding pressure cells
         hs(i,j)=2*(sxx(i,j)*exx(i,j)+sxx(i+1,j)*exx(i+1,j)+sxx(i,j+1)*exx(i,j+1)+sxx(i+1,j+1)*exx(i+1,j+1))/4+2*sxy(i,j)*exy(i,j);
         % Adiabatic heating Ha=ALPHA*T*DP/Dt~ALPHA*T*gy*RHO*vy
         % using vy velocity for internal basic nodes
         ha(i,j)=ha(i,j)*gy*rho(i,j)*vy1(i,j);
      end
    end
    
    % Computing total heating
    htotal=hr+ha+hs;
    
    
    % Check maximal velocity
    vxmax=max(max(abs(vx)));
    vymax=max(max(abs(vy)));
    % Set initial value for the timestep
    timestep=timemax;
    % Check marker displacement step
    if (vxmax>0)
        if (timestep>markmax*xstp/vxmax);
            timestep=markmax*xstp/vxmax;
        end
    end
    if (vymax>0)
        if (timestep>markmax*ystp/vymax);
            timestep=markmax*ystp/vymax;
        end
    end
    
    
    % Solving temperature equation by implicit method
    yn=0; % Temperature solution with the correct timestep, Yes/No
    while (yn<=0)
        % Matrix of coefficients initialization for implicit solving
        L=sparse(xnum*ynum,xnum*ynum);
        % Vector of right part initialization for implicit solving
        R=zeros(xnum*ynum,1);

        % Implicit solving of 2D temperature equation:
        % RHO*Cp*dT/dt=d(k*dT/dx)/dx+d(k*dT/dy)/dy
        % Composing matrix of coefficients L()
        % and vector (column) of right parts R()
        % Process all grid points
        for i=1:1:ynum
            for j=1:1:xnum
                % Global index
                k=(j-1)*ynum+i;
                % Boundary nodes
                if(i==1 || i==ynum || j==1 || j==xnum)
                    % Upper boundary
                    if(i==1)
                        % Constant temperature
                        L(k,k)=1;
                        R(k,1)=tktop;
                        % Insulating boundary: T(i,j)=T(i+1,j)
                        %L(k,k)=1;
                        %L(k,k+1)=-1;
                        %R(k,1)=0;                    
                    end
                    % Lower boundary
                    if(i==ynum)
                        % Constant temperature
                        L(k,k)=1;
                        R(k,1)=tktop;
                        % Insulating boundary: T(i,j)=T(i-1,j)
                        %L(k,k)=1;
                        %L(k,k-1)=-1;
                        %R(k,1)=0;                    
                    end
                    % Left boundary
                    if(j==1 && i>1 && i<ynum)
                        % Constant temperature
                        L(k,k)=1;
                        R(k,1)=tktop;
                        % Insulating boundary: T(i,j)=T(i,j+1)
                        %L(k,k)=1;
                        %L(k,k+ynum)=-1;
                        %R(k,1)=0;                    
                    end
                    % Right boundary
                    if(j==xnum && i>1 && i<ynum)
                        % Constant temperature
                        L(k,k)=1;
                        R(k,1)=tktop;
                        % Insulating boundary: T(i,j)=T(i,j-1)
                        %L(k,k)=1;
                        %L(k,k-ynum)=-1;
                        %R(k,1)=0;                    
                    end
                % Internal nodes
                else
                    % RHO*Cp*dT/dt=d(k*dT/dx)/dx+d(k*dT/dy)/dy
                    % Left part
                    % -d(k*dT/dx)/dx
                    L(k,k-ynum)=-(kt(i,j-1)+kt(i,j))/2/xstp^2; % coefficient for T(i,j-1)
                    L(k,k+ynum)=-(kt(i,j+1)+kt(i,j))/2/xstp^2; % coefficient for T(i,j+1)
                    L(k,k)=(kt(i,j-1)+kt(i,j))/2/xstp^2+(kt(i,j+1)+kt(i,j))/2/xstp^2; % coefficient for T(i,j+1)
                    % -d(k*dT/dy)/dy
                    L(k,k-1)=-(kt(i-1,j)+kt(i,j))/2/ystp^2; % coefficient for T(i-1,j)
                    L(k,k+1)=-(kt(i+1,j)+kt(i,j))/2/ystp^2; % coefficient for T(i+1,j)
                    L(k,k)=L(k,k)+(kt(i-1,j)+kt(i,j))/2/ystp^2+(kt(i+1,j)+kt(i,j))/2/ystp^2; % ADD coefficient for T(i,j)
                    % RHO*Cp*(dT/dt)
                    L(k,k)=L(k,k)+rhocp(i,j)/timestep; % ADD coefficient for T(i,j)
                    % Right part
                    R(k,1)=rhocp(i,j)*tk1(i,j)/timestep+hr(i,j)+ha(i,j)+hs(i,j);
                end
            end
        end
        % Obtaining solution
        S=L\R;
        % Reloading solution to the new temperature array
        tk2=tk1;
        for i=1:1:ynum
            for j=1:1:xnum
                % Global index
                k=(j-1)*ynum+i;
                % Reload solution
                tk2(i,j)=S(k);
            end
        end
        % Computing temperature changes
        dtk1=tk2-tk1;
        % Compute maximal temperature changes
        dtkmax=max(max(abs(dtk1)));
        % After first solution
        if(yn==0)
            if (dtkmax<=tempmax)
                % Timestep was OK
                yn=1; % Do not repeat temperature solution 
            else
                % Timestep was too big
                % Reduce timestep
                timestep=timestep*tempmax/dtkmax;
                yn=-1; % Repeat temperature solution
            end
        % After second solution
        else
            yn=1; % Do not repeat temperature solution anymore 
        end
    end
    
    % Computing subgrid diffusion for markers
    if (dsubgridt>0)
        % Clear subgrid temperature changes for nodes
        dtkn=zeros(ynum,xnum);
        % Clear wights for basic nodes
        wtnodes=zeros(ynum,xnum);
        % Marker cycle
        for mm1 = 1:1:marknum
            % Check markers inside the grid
            if (MX(mm1)>=0 && MX(mm1)<=xsize && MY(mm1)>=0 && MY(mm1)<=ysize) 

                %  yn    T(yn,xn)--------------------T(yn,xn+1)
                %           ?           ^                  ?
                %           ?           ?                  ?
                %           ?          dy                  ?
                %           ?           ?                  ?
                %           ?           v                  ?
                %           ?<----dx--->o MTK(mm1)         ?
                %           ?                              ?
                %           ?                              ?
                %  yn+1  T(yn+1,xn)-------------------V(yn+1,xn+1)
                %
                %
                % Interpolating temperature changes from basic nodes
                % !!! SUBTRACT 0.5 since int16(0.5)=1
                xn=double(int16(MX(mm1)./xstp-0.5))+1;
                yn=double(int16(MY(mm1)./ystp-0.5))+1;
                % Check horizontal index for upper left T-node 
                % It must be between 1 and xnum-1 (see picture for grid)
                if (xn<1)
                    xn=1;
                end
                if (xn>xnum-1)
                    xn=xnum-1;
                end
                if (yn<1)
                    yn=1;
                end
                if (yn>ynum-1)
                    yn=ynum-1;
                end
                % Define and check normalized distances from marker to the upper left T-node;
                dx=MX(mm1)./xstp-xn+1;
                dy=MY(mm1)./ystp-yn+1;
                % Interpolate old nodal temperature for the marker
                tkm=0;
                tkm=tkm+(1.0-dx).*(1.0-dy).*tk1(yn,xn);
                tkm=tkm+(1.0-dx).*dy.*tk1(yn+1,xn);
                tkm=tkm+dx.*(1.0-dy).*tk1(yn,xn+1);
                tkm=tkm+dx.*dy.*tk1(yn+1,xn+1);
                % Calculate Nodal-Marker subgrid temperature difference
                dtkm=tkm-MTK(mm1);
                % Compute nodal k and RHO*Cp from the grid for the marker location 
                % k
                ktm=0;
                ktm=ktm+(1.0-dx).*(1.0-dy).*kt(yn,xn);
                ktm=ktm+(1.0-dx).*dy.*kt(yn+1,xn);
                ktm=ktm+dx.*(1.0-dy).*kt(yn,xn+1);
                ktm=ktm+dx.*dy.*kt(yn+1,xn+1);
                % RHO*Cp
                rhocpm=0;
                rhocpm=rhocpm+(1.0-dx).*(1.0-dy).*rhocp(yn,xn);
                rhocpm=rhocpm+(1.0-dx).*dy.*rhocp(yn+1,xn);
                rhocpm=rhocpm+dx.*(1.0-dy).*rhocp(yn,xn+1);
                rhocpm=rhocpm+dx.*dy.*rhocp(yn+1,xn+1);

                % Compute local thermal diffusion timescale for the marker
                tdm=rhocpm/ktm/(2/xstp^2+2/ystp^2);

                % Computing subgrid diffusion
                sdif=-dsubgridt*timestep/tdm;
                if(sdif<-30) 
                    sdif=-30;
                end
                dtkm=dtkm*(1-exp(sdif));    

                % Correcting old temperature for the marker
                MTK(mm1)=MTK(mm1)+dtkm;

                % Interpolating subgrid temperature changes to 4
                dtkn(yn,xn)=dtkn(yn,xn)+(1.0-dx).*(1.0-dy).*dtkm;
                wtnodes(yn,xn)=wtnodes(yn,xn)+(1.0-dx).*(1.0-dy);
                dtkn(yn+1,xn)=dtkn(yn+1,xn)+(1.0-dx).*dy.*dtkm;
                wtnodes(yn+1,xn)=wtnodes(yn+1,xn)+(1.0-dx).*dy;
                dtkn(yn,xn+1)=dtkn(yn,xn+1)+dx.*(1.0-dy).*dtkm;
                wtnodes(yn,xn+1)=wtnodes(yn,xn+1)+dx.*(1.0-dy);
                dtkn(yn+1,xn+1)=dtkn(yn+1,xn+1)+dx.*dy.*dtkm;
                wtnodes(yn+1,xn+1)=wtnodes(yn+1,xn+1)+dx.*dy;

            end
        end

        % Computing subgrid diffusion for nodes
        for i=1:1:ynum;
            for j=1:1:xnum;
                % Density
                if (wtnodes(i,j)~=0)
                    % Compute new value interpolated from markers
                    dtkn(i,j)=dtkn(i,j)./wtnodes(i,j);
                end
            end
        end

        % Subtracting subgrid diffusion part from nodal temperature changes
        dtk1=dtk1-dtkn;

    end

    % Updating temperature for markers
    for mm1 = 1:1:marknum
        % Check markers inside the grid
        if (MX(mm1)>=0 && MX(mm1)<=xsize && MY(mm1)>=0 && MY(mm1)<=ysize) 

            %  yn    T(yn,xn)--------------------T(yn,xn+1)
            %           ?           ^                  ?
            %           ?           ?                  ?
            %           ?          dy                  ?
            %           ?           ?                  ?
            %           ?           v                  ?
            %           ?<----dx--->o MTK(mm1)         ?
            %           ?                              ?
            %           ?                              ?
            %  yn+1  T(yn+1,xn)-------------------V(yn+1,xn+1)
            %
            %
            % Interpolating temperature changes from basic nodes
            % !!! SUBTRACT 0.5 since int16(0.5)=1
            xn=double(int16(MX(mm1)./xstp-0.5))+1;
            yn=double(int16(MY(mm1)./ystp-0.5))+1;
            % Check horizontal index for upper left T-node 
            % It must be between 1 and xnum-1 (see picture for grid)
            if (xn<1)
                xn=1;
            end
            if (xn>xnum-1)
                xn=xnum-1;
            end
            if (yn<1)
                yn=1;
            end
            if (yn>ynum-1)
                yn=ynum-1;
            end
            % Define and check normalized distances from marker to the upper left T-node;
            dx=MX(mm1)./xstp-xn+1;
            dy=MY(mm1)./ystp-yn+1;
            % Calculate Marker temperature change from four surrounding nodes
            dtkm=0;
            dtkm=dtkm+(1.0-dx).*(1.0-dy).*dtk1(yn,xn);
            dtkm=dtkm+(1.0-dx).*dy.*dtk1(yn+1,xn);
            dtkm=dtkm+dx.*(1.0-dy).*dtk1(yn,xn+1);
            dtkm=dtkm+dx.*dy.*dtk1(yn+1,xn+1);
            %
            %Computing new temperature for the marker
            MTK(mm1)=MTK(mm1)+dtkm;

        end
    end
   
    
    
    % Moving Markers by velocity field
    if(markmove>0)
        % Create arrays for velocity of markers
        vxm=zeros(4,1);
        vym=zeros(4,1);
        % Marker cycle
        for mm1 = 1:1:marknum

            % Check markers inside the grid
            if (MX(mm1)>=gridx(1) && MX(mm1)<=gridx(xnum) && MY(mm1)>=gridy(1) && MY(mm1)<=gridy(ynum)) 

                % Save marker coordinates
                xcur=MX(mm1);
                ycur=MY(mm1);
                % Defining number of Runge-Kutta cycles
                for rk=1:1:markmove

                    %  xn    V(xn,yn)--------------------V(xn+1,yn)
                    %           ?           ^                  ?
                    %           ?           ?                  ?
                    %           ?          dy                  ?
                    %           ?           ?                  ?
                    %           ?           v                  ?
                    %           ?<----dx--->o Mrho(xm,ym)      ?
                    %           ?                              ?
                    %           ?                              ?
                    %  xn+1  V(xn,yn+1)-------------------V(xn+1,yn+1)

                    % Define indexes for upper left node in the VX-cell where the marker is
                    % VX-cells are displaced downward for 1/2 of vertical gridstep
                    % !!! SUBTRACT 0.5 since int16(0.5)=1
                    xn=double(int16(xcur./xstp-0.5))+1;
                    yn=double(int16((ycur-ystp/2.0)./ystp-0.5))+1;
                    % Check indexes:
                    % vertical index for upper left VX-node must be between 1 and ynum-2
                    if (xn<1)
                        xn=1;
                    end
                    if (xn>xnum-1)
                        xn=xnum-1;
                    end
                    if (yn<1)
                        yn=1;
                    end
                    if (yn>ynum-2)
                        yn=ynum-2;
                    end

                    % Define and check normalized distances from marker to the upper left VX-node;
                    dx=(xcur-gridx(xn))./xstp;
                    dy=(ycur-gridcy(yn))./ystp;

                    % Calculate Marker velocity from four surrounding Vx nodes
                    vxm(rk)=0;
                    vxm(rk)=vxm(rk)+(1.0-dx).*(1.0-dy).*vx(yn,xn);
                    vxm(rk)=vxm(rk)+(1.0-dx).*dy.*vx(yn+1,xn);
                    vxm(rk)=vxm(rk)+dx.*(1.0-dy).*vx(yn,xn+1);
                    vxm(rk)=vxm(rk)+dx.*dy.*vx(yn+1,xn+1);

                    % Define indexes for upper left node in the VY-cell where the marker is
                    % VY-cells are displaced rightward for 1/2 of horizontal gridstep
                    % !!! SUBTRACT 0.5 since int16(0.5)=1
                    xn=double(int16((xcur-xstp/2.0)./xstp-0.5))+1;
                    yn=double(int16(ycur./ystp-0.5))+1;
                    % Check indexes:
                    % horizontal index for upper left VY-node must be between 1 and xnum-2
                    if (xn<1)
                        xn=1;
                    end
                    if (xn>xnum-2)
                        xn=xnum-2;
                    end
                    if (yn<1)
                        yn=1;
                    end
                    if (yn>ynum-1)
                        yn=ynum-1;
                    end

                    % Define and check normalized distances from marker to the upper left VY-node;
                    dx=(xcur-gridcx(xn))./xstp;
                    dy=(ycur-gridy(yn))./ystp;

                    % Calculate Marker velocity from four surrounding nodes
                    vym(rk)=0;
                    vym(rk)=vym(rk)+(1.0-dx).*(1.0-dy).*vy(yn,xn);
                    vym(rk)=vym(rk)+(1.0-dx).*dy.*vy(yn+1,xn);
                    vym(rk)=vym(rk)+dx.*(1.0-dy).*vy(yn,xn+1);
                    vym(rk)=vym(rk)+dx.*dy.*vy(yn+1,xn+1);

                    % Update coordinates for the next Runge-Kutta cycle
                    if(rk<4)
                        if (rk<3)
                            xcur=MX(mm1)+timestep/2*vxm(rk);
                            ycur=MY(mm1)+timestep/2*vym(rk);
                        else
                            xcur=MX(mm1)+timestep*vxm(rk);
                            ycur=MY(mm1)+timestep*vym(rk);
                        end
                    end
                end
                % Recompute velocity using 4-th order Runge_Kutta
                if (markmove==4)
                    vxm(1)=(vxm(1)+2*vxm(2)+2*vxm(3)+vxm(4))/6;
                    vym(1)=(vym(1)+2*vym(2)+2*vym(3)+vym(4))/6;
                end

                % Displacing Marker according to its velocity
                MX(mm1)=MX(mm1)+timestep*vxm(1);
                MY(mm1)=MY(mm1)+timestep*vym(1);
                
            end
        end
    end
    
    % Advance in time
    timesum=timesum+timestep;
    
    display(['advance in time by ',num2str(timestep/(3600.000*24.000*365.250*1.000e6)),' Ma'])
    display(' ')
    
    pause(0.1)
    
end


