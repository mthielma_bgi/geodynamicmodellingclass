%==================================================================
%
%                 THIS IS FD_HEAT_1D_IMPLICIT.M
%
% IT IS AN EDUCATIONAL CODE THAT DEMONSTRATES HOW TO SOLVE THE 1D HEAT
% EQUATION IN 1D WITH FINITE DIFFERENCES AND IMPLICIT TIME STEPPING
%
%      dT/dt   =  d/dx ( kappa(x)   dT/dx )
%
% Marcel Thielmann, October 2015
%
%===================================================================

clearvars % delete all variables in workspace
close all % close all figures


% PHYSICAL PARAMETERS
%======================
L       = ???;            % length of domain
kappa   = ???;             % diffusivity
dt      = ???;       % time step
nstep   = ???;             % number of time steps

Tmax    = ???;
sigma   = ???;

% left and right boundary conditions - this is a special matlab way (called
% a "function handle" to prescribe a time-dependent function to the
% boundary in this case (which is the analytical solution)
T_left  = @(t) Tmax/(sqrt(1+4*t*kappa/sigma^2)).*exp(-(-0.5*L).^2./(sigma^2 + 4*kappa*t));
T_right = @(t) Tmax/(sqrt(1+4*t*kappa/sigma^2)).*exp(-(0.5*L).^2./(sigma^2 + 4*kappa*t));

% NUMERICAL PARAMETERS
%======================
nnodes  = ???; % number of nodes

%======================================================================
% RUN THE SIMULATION
%======================================================================
% discretize the system
dx      = L/(nnodes-1);
x_vec   = [-0.5*L:dx:0.5*L]';

% set up the initial temperature
T       = Tmax.*exp(-x_vec.^2./sigma^2);

% precompute necessary parameters
s       = kappa*dt./dx^2;

% initialize new temperature vector
T_new = T;
time  = 0;

time_vec    = 0:dt:nstep*dt;
L2          = ones(nstep+1,1)*NaN;
L2(1)       = 0;

% set up the matrix
A = sparse(nnodes,nnodes);

A(1,1)              = 1;
A(nnodes,nnodes)    = 1;

for inode = 2:nnodes-1
    A(inode,inode-1)    = -s;
    A(inode,inode)      = 1+2*s;
    A(inode,inode+1)    = -s;
end

for istep = 1:nstep
    
    % set up Rhs
    Rhs          = T;
    Rhs(1)       = T_left(time+dt);
    Rhs(nnodes)  = T_right(time+dt);
    
    %solve
    T_new        = A\Rhs;
    
    % update temperature and time
    T            = T_new;
    time         = time+dt;
    
    % compute analytical temperature profile
    T_ana        = ???;
    Misfit       = (T-T_ana);
    L2(istep+1)  = sqrt(sum((Misfit*dx).^2))./L;
    
    % plot numerical and analytical solution
    % also plot misfit per node and L2 norm
    if mod(istep,10)==0
        figure(1),clf
        subplot(311)
        plot(x_vec,T,'r-')
        hold on
        plot(x_vec,T_ana,'b-')
        subplot(312)
        plot(x_vec,Misfit,'r-')
        subplot(313)
        plot(time_vec,L2,'-o')
        drawnow
        bla=1;
    end
    
end
