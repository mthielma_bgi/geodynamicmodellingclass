% This shows how two heavy block falls down in a lower viscous fluid

% Add paths & subdirectories
addpath(genpath('~/Desktop/WORK/MILAMIN/MILAMIN_VEP2/'))
addpath(genpath('~/Desktop/WORK/MILAMIN/SCRIPTS/MESH_CREATION'))
AddDirectories;

clearvars

%% General setup parameters
SETUP.SphereRadius      = 150e3;
SETUP.SphereTemperature = 1700;
SETUP.MantleTemperature = 1600;
SETUP.SphereDepth       = -2500e3;

SecYear                 = 365.25*24*3600; % seconds in one year
NUMERICS.dt             = 2e3*SecYear;

%% Create mesh
CreatePlots         =   logical(1);

mesh_input.x_min    =   -1500e3; % minimum x-value
mesh_input.x_max    =    1500e3; % maximum x-value
mesh_input.z_min    =   -3000e3; % minimum z-value
mesh_input.z_max    =   0; % maximum z-value

opts.element_type = 'quad9';
% opts.element_type   = 'quad4';      

opts.nx             =   101;           % number of nodes in x-direction
opts.nz             =   101;           % number of nodes in z-direction
 
%% Set material properties for every phase
% Note: parameters that are NOT specified here, but are required for the
% simulation are set to default values

%----------------------------------
% Fluid
Phase                                           =   1;
Type                                            =   'Constant';
MATERIAL_PROPS(Phase).Viscosity.(Type).Mu       =   1e20;                   % parameter

% Type                                            =   'Powerlaw';                    % type of viscosity law we can employ
% MATERIAL_PROPS(Phase).Viscosity.(Type).Mu0      =   1e21;                          % parameter
% MATERIAL_PROPS(Phase).Viscosity.(Type).n        =   1;
% MATERIAL_PROPS(Phase).Viscosity.(Type).e0       =   1e-12;

Type                                            =   'TemperatureDependent'; % type of density law we can employ
MATERIAL_PROPS(Phase).Density.(Type).Rho0       =   3000;                   % parameter
MATERIAL_PROPS(Phase).Density.(Type).T0         =   SETUP.MantleTemperature;                   % parameter


MATERIAL_PROPS(Phase).HeatCapacity.Constant.Cp = 1050;
MATERIAL_PROPS(Phase).Conductivity.Constant.k  = 3;

%
%----------------------------------
MATERIAL_PROPS(1).Gravity.Value                 =   9.81;

% Add material properties that were not defined here, but which MVEP2 needs to know
MATERIAL_PROPS                          =    AppendMaterialProps(MATERIAL_PROPS,0);      	% Set default parameters if required

%% Numerics
NUMERICS.Viscosity.LowerCutoff                              =   1e18;
NUMERICS.Viscosity.UpperCutoff                              =   1e24;
NUMERICS.Timestepping.Method                                =   'Euler';
NUMERICS.InterpolationMethod.ParticlesToIntegrationPoints   = 'constant';
NUMERICS.LinearSolver.PressureShapeFunction                 =  'Local';
NUMERICS.LinearSolver.StaticPressureCondensation            = true;
NUMERICS.LinearSolver.UseSuiteSparse                        = true;
NUMERICS                                                    =   SetDefaultNumericalParameters(NUMERICS);    % add default parameters if required

%% Set boundary conditions
% Stokes
Bound                   =   {'No Slip','Free Slip','Constant Strainrate', 'No Stress'};
BC.Stokes.Bottom        =   Bound{2};
BC.Stokes.Left          =   Bound{2};
BC.Stokes.Right         =   Bound{2};
BC.Stokes.Top           =   Bound{2};
BC.Stokes.BG_strrate    =   0;

% Thermal boundary condition
%                            1              2           
BoundThermal          =   {'Zero flux', 'Isothermal'};
BC.Energy.Top         =   BoundThermal{2};
BC.Energy.Bottom      =   BoundThermal{2};
BC.Energy.Left        =   BoundThermal{1};
BC.Energy.Right       =   BoundThermal{1};
BC.Energy.Value.Top   =   SETUP.MantleTemperature;
BC.Energy.Value.Bottom=   SETUP.MantleTemperature;                    % T at bottom of mantle


%% Create Particles
n_markers               =   1e6;
[X, Z]                  =   meshgrid(   linspace(mesh_input.x_min,mesh_input.x_max,ceil(sqrt(n_markers))), ...
                                        linspace(mesh_input.z_min,mesh_input.z_max,ceil(sqrt(n_markers)))   );
dx_part             	=   mean(diff(unique(X(:))));
dz_part                 =   mean(diff(unique(Z(:))));
PARTICLES.x             =   X(:)' + 1*(rand(size(X(:)'))-0.5)*dx_part;                         
PARTICLES.z             =   Z(:)' + 1*(rand(size(X(:)'))-0.5)*dz_part;  
PARTICLES.phases        =   ones(size(PARTICLES.x),'uint32');
PARTICLES.HistVar.T     =   0*PARTICLES.z+SETUP.MantleTemperature;                        % temperature
PARTICLES.PlotFlag      =   ones(size(PARTICLES.x),'uint8');


% % set initial particles distribution
% Sphere
ind = find(PARTICLES.x.^2+(PARTICLES.z-SETUP.SphereDepth).^2<=SETUP.SphereRadius.^2);
PARTICLES.HistVar.T(ind) = SETUP.SphereTemperature;
PARTICLES.PlotFlag(ind)  = uint8(2);

%% Non-dimensionalize input parameters
CHAR.Length         = SETUP.SphereRadius;
CHAR.Viscosity      = 1e21;
CHAR.Temperature    = SETUP.MantleTemperature;
CHAR.Time           = 1e15;
[mesh_input, BC, MATERIAL_PROPS, PARTICLES, CHAR, NUMERICS]    =  NonDimensionalizeParameters(mesh_input, BC, MATERIAL_PROPS, PARTICLES,  NUMERICS, CHAR); 


%% Load breakpoint file if required
Load_BreakpointFile


%% Create mesh
[MESH]              =   CreateMesh(opts, mesh_input);

%% Plot initial particles
if 1==1 & CreatePlots
    figure(1), clf, hold on
    plotclr(PARTICLES.x,PARTICLES.z,PARTICLES.HistVar.T)
    axis equal, axis tight
end

dt      =   NUMERICS.dt;   % initial timestep
time 	=    0;     % initial time
for itime=NUMERICS.time_start:1e3
    start_cpu = cputime;
     
    %% Create finite element mesh
    [MESH]              =   CreateMesh(opts, mesh_input, MESH);

    %% Compute Stokes (& Energy solution if required)
    [PARTICLES, MESH, INTP_PROPS, NUMERICS] 	 = StokesEnergy_2D(MESH, BC, PARTICLES, MATERIAL_PROPS, dt, NUMERICS, CHAR);
    
     
    %% Advect particles [MESH is Eulerian in this simulation]
    PARTICLES.x     =   PARTICLES.x + PARTICLES.Vx*NUMERICS.dt;
    PARTICLES.z     =   PARTICLES.z + PARTICLES.Vz*NUMERICS.dt;
    MESH.NODES      =   MESH.NODES  + MESH.VEL*dt;
    
    time            =   time+NUMERICS.dt;
    
    %% Compute new timestep 
    CFL             =   .25;
    dt              =   CourantTimestep(MESH, CFL);
    
    %% Plot results
    if mod(itime,1)==0 & CreatePlots
        if isfield(MESH,'TEMP')
            figure(1), clf
            PlotMesh(MESH,MESH.TEMP*CHAR.Temperature, CHAR.Length/1e3); axis equal, axis tight, colorbar; shading interp
            colorbar, title(['Temperature [K]'])
            caxis([SETUP.MantleTemperature SETUP.SphereTemperature])
            
            figure(2),clf
            ind2 = find(PARTICLES.PlotFlag==uint8(2));
            plot(PARTICLES.x(ind2)*CHAR.Length/1e3,PARTICLES.z(ind2)*CHAR.Length/1e3,'r.')
            
            hold on
            quiver(MESH.NODES(1,:)*CHAR.Length/1e3,MESH.NODES(2,:)*CHAR.Length/1e3,MESH.VEL(1,:),MESH.VEL(2,:));
            axis equal, axis tight
            title(['Time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),'Myrs'])
            xlabel('Width [km]')
            ylabel('Depth [km]')
            
            drawnow
        end

        %pause(0.5)
        
%         figure(6), clf
%         X=MESH.NODES(1,:);Z=MESH.NODES(2,:);
%         X2d = X(MESH.RegularGridNumber);
%         Z2d = Z(MESH.RegularGridNumber);
%         plot(X2d(end,:),Z2d(end,:)*1e3)
%         xlabel('Width [km]')
%         ylabel('Topography [m]')
%         drawnow
       
    end
   
    
    % Create breakpoint file if required
    Save_BreakpointFile;
    
    
    % Save filename if required
    if mod(itime,5)==0
        %SaveFile(itime, PARTICLES, NUMERICS,MESH,INTP_PROPS,CHAR,BC, MATERIAL_PROPS, time);
    end
 
    
    end_cpu         = cputime;
    time_timestep   = end_cpu-start_cpu;
    
    disp(['Timestep ',num2str(itime), ' took ',num2str(time_timestep),'s and reached time=',num2str(time*CHAR.Time/CHAR.SecYear/1e6),' Myrs'])
    disp(' ')
    
end
