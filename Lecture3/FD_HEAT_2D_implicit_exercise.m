%==================================================================
%
%                 THIS IS FD_HEAT_2D_IMPLICIT.M
%
% IT IS AN EDUCATIONAL CODE THAT DEMONSTRATES HOW TO SOLVE THE 2D HEAT
% EQUATION IN 2D WITH FINITE DIFFERENCES AND IMPLICIT TIME STEPPING
%
%      dT/dt   =  d/dx ( kappa   dT/dx )
%
% Marcel Thielmann, October 2015
%
%===================================================================
clearvars
close all

% MATERIAL PROPERTIES
%================================
MATERIAL_PROPS.kappa = [???];

% INITIAL TEMPERATURE SETUP
%===============================
SETUP.sigma     = ???;
SETUP.Tmax      = ???;

% MESH PARAMETERS
%===============================
MESH.x_min       = ???;
MESH.x_max       = ???;
MESH.y_min       = ???;
MESH.y_max       = ???;
MESH.nodes_x     = ???;
MESH.nodes_y     = ???;
MESH.nnodes      = MESH.nodes_x*MESH.nodes_y;
% NUMERICAL PARAMETERS
%================================
NUMERICS.nt      = ???; % number of time steps
NUMERICS.dt      = ???; % size of time step

% BOUNDARY CONDITIONS
%================================
BC.left     = @(x,y,t) SETUP.Tmax/(sqrt(1+4*t*MATERIAL_PROPS.kappa/SETUP.sigma^2)).*exp(-(x.^2+y.^2)./(SETUP.sigma^2 + 4*MATERIAL_PROPS.kappa*t));
BC.right    = @(x,y,t) SETUP.Tmax/(sqrt(1+4*t*MATERIAL_PROPS.kappa/SETUP.sigma^2)).*exp(-(x.^2+y.^2)./(SETUP.sigma^2 + 4*MATERIAL_PROPS.kappa*t));
BC.top      = @(x,y,t) SETUP.Tmax/(sqrt(1+4*t*MATERIAL_PROPS.kappa/SETUP.sigma^2)).*exp(-(x.^2+y.^2)./(SETUP.sigma^2 + 4*MATERIAL_PROPS.kappa*t));
BC.bottom   = @(x,y,t) SETUP.Tmax/(sqrt(1+4*t*MATERIAL_PROPS.kappa/SETUP.sigma^2)).*exp(-(x.^2+y.^2)./(SETUP.sigma^2 + 4*MATERIAL_PROPS.kappa*t));

% MESH CREATION
%================================

MESH.Lx = MESH.x_max-MESH.x_min;
MESH.Ly = MESH.y_max-MESH.y_min;
MESH.dx = MESH.Lx/(MESH.nodes_x-1);
MESH.dy = MESH.Lx/(MESH.nodes_y-1);
[MESH.x,MESH.y] = meshgrid(MESH.x_min:MESH.dx:MESH.x_max,MESH.y_min:MESH.dy:MESH.y_max);
MESH.Number     = 0*MESH.x;
MESH.Number(:)  = 1:length(MESH.Number(:));

% set point ids for mesh nodes
%--------------------------------
MESH.Point_id  = zeros(size(MESH.x)); 

% left boundary - 1
MESH.Point_id(MESH.x==MESH.x_min) = 1;
% right boundary
MESH.Point_id(MESH.x==MESH.x_max) = 2;
% bottom boundary
MESH.Point_id(MESH.y==MESH.y_min) = 3;
% top boundary
MESH.Point_id(MESH.y==MESH.y_max) = 4;



% Note that there is no special numbering for corners, thus corners will be
% seen as bottom or top. You can change that of course.


% set initial temperature
%=======================================
T  = SETUP.Tmax.*exp(-(MESH.x.^2+MESH.y.^2)./SETUP.sigma^2);



% set up system of equations
%=======================================
sx = MATERIAL_PROPS.kappa.*NUMERICS.dt./(MESH.dx^2);
sy = MATERIAL_PROPS.kappa.*NUMERICS.dt./(MESH.dy^2);
% loop over different point_ids and create triplets, take into account
% different boundary conditions
% point_id : 0 - normal, 1 - left, 2 - right, 3 - bottom, 4 - top

% for each node there are five triplets -> preallocate them
triplets = ones(5*MESH.nnodes,3);

% set triplets
% NOTE: MATLAB takes the first index in a matrix as the row index and the
% second one as the column index !!!
itriplet = 1;
for i = 1:MESH.nodes_x
    for j = 1:MESH.nodes_y
        
        if MESH.Point_id(j,i) == 0 % normal stencil on all nodes without boundary condition
            RowIndex = MESH.Number(j,i); % stays the same
            
            ColIndex = MESH.Number(j,i+1);
            triplets(itriplet,:) = [RowIndex,ColIndex,???];
            itriplet = itriplet+1;
            
            ColIndex = MESH.Number(j,i-1);
            triplets(itriplet,:) = [RowIndex,ColIndex,???];
            itriplet = itriplet+1;
            
            ColIndex = MESH.Number(j,i);
            triplets(itriplet,:) = [RowIndex,ColIndex,???];
            itriplet = itriplet+1;
            
            ColIndex = MESH.Number(j+1,i);
            triplets(itriplet,:) = [RowIndex,ColIndex,???];
            itriplet = itriplet+1;
            
            ColIndex = MESH.Number(j-1,i);
            triplets(itriplet,:) = [RowIndex,ColIndex,???];
            itriplet = itriplet+1;
        else
            % here we assume that all boundaries are constant temperature
            RowIndex = MESH.Number(j,i);
            ColIndex = MESH.Number(j,i);
            triplets(itriplet,:) = [RowIndex,ColIndex,1];
            itriplet = itriplet+1;
        end
    end
end
triplets(itriplet:end,:) = []; % clean up the triplet vector

% assemble the matrix (as there is no deformation, we only have to do this
% once
A = sparse(triplets(:,1),triplets(:,2),triplets(:,3));

% initialize needed variables

time = 0;
for it = 1:NUMERICS.nt
    
    % set up rhs
    Rhs  = T(:); % here we use the fact that matlab uses linear indexing
    
    % set BCs
    ind_left    = find(MESH.Point_id==1);
    ind_right   = find(MESH.Point_id==2);
    ind_bottom  = find(MESH.Point_id==3);
    ind_top     = find(MESH.Point_id==4);
    
    
    Rhs(MESH.Number(ind_left))    = BC.left(MESH.x(ind_left),MESH.y(ind_left),time+0.5*NUMERICS.dt);
    Rhs(MESH.Number(ind_right))   = BC.right(MESH.x(ind_right),MESH.y(ind_right),time+0.5*NUMERICS.dt);
    Rhs(MESH.Number(ind_bottom))  = BC.bottom(MESH.x(ind_bottom),MESH.y(ind_bottom),time+0.5*NUMERICS.dt);
    Rhs(MESH.Number(ind_top))     = BC.top(MESH.x(ind_top),MESH.y(ind_top),time+0.5*NUMERICS.dt);
    
    
    % solve the system
    T_new = A\Rhs;
    
    % put T_new in matrix format using the numbering
    T_new = T_new(MESH.Number);
    
    % compare to analytical solution

    % plot
    if mod(it,10)==0
        figure(1),clf
        surf(MESH.x,MESH.y,T_new),shading interp,colorbar
        zlim([0 1000])
        caxis([0 1000])
        drawnow
    end
    
    
    % update
    time    = time+NUMERICS.dt;
    T       = T_new;
    

end


